import React from 'react';
import ReactDOM from 'react-dom';

import { Fragment } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';

import RoomTable from './RoomsTable';

import './style.scss';
import logo from '../src/logo.svg';
import account from '../src/account.svg';

export class App extends React.Component {

	render() {
		return (
			<Fragment>
				<header>
					<div><img className='logo' src={logo} alt='Ghost...' />CASPER</div>
					<span>User with long username <img className='account' src={account} alt='Ghost...' /></span>
				</header>
				<BrowserRouter>
					<Route exact path='/' component={RoomTable} />
				</BrowserRouter>
			</Fragment>
		);
	}
}