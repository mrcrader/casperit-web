import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Link } from 'react-router-dom';

import Marks from './Marks';

import './style.scss';

export default class RoomTable extends React.Component {

	constructor() {
		super();

		this.state = {
			activeStreet: 'Программистов, 3',
			streets: ['Программистов, 3', 'Купревича, 14', 'Кульман, 1', 'M. Богдановича, 155']
		};

		this.setActiveStreet = this.setActiveStreet.bind(this);
	}
	componentDidMount() {

	}

	setActiveStreet(elem) {
		this.setState({ activeStreet: elem.target.value });
	}

	render() {
		return (
			<div className='main'>
				<div className='addressNavigation'>
					{this.state.streets.map((elem) => {
						console.log(elem);
						let classForElem = elem.includes(this.state.activeStreet) ? 'active' : '';
						return <input type='button' className={classForElem} onClick={this.setActiveStreet} value={elem} />
					})
					}
				</div>
				<div className='workspace'>
					<div className='marksBar'>
						<span>{Marks.bookedCircle} Занято</span>
						<span>{Marks.emptyCircle} Свободно</span>
						<span>{Marks.onApproveCircle} На подтверждении</span>
					</div>

					<div className='meetroomsTable'>
						<div className='header'>
							<span className='rn'>№ комнаты</span>
							<span className='st'>Статус</span>
							<span className='cn'>Оснащение</span>
							<span className='vt'>Кол-во персон</span>
						</div>

						<div className='row'>
							<span className='rn'>Test</span>
							<span className='st'>{Marks.emptyCircle}</span>
							<span className='cn'>Lalala</span>
							<span className='vt'>You can't touch this</span>
							<input type='button' className='bookButton' value='Забронировать' />
						</div>

						<div className='row'>
							<span className='rn'>Test</span>
							<span className='st'>{Marks.emptyCircle}</span>
							<span className='cn'>Lalala</span>
							<span className='vt'>You can't touch this</span>
							<input type='button' className='bookButton' value='Забронировать' />
						</div>

						<div className='row'>
							<span className='rn'>Test</span>
							<span className='st'>{Marks.emptyCircle}</span>
							<span className='cn'>Lalala</span>
							<span className='vt'>You can't touch this</span>
							<input type='button' className='bookButton' value='Забронировать' />
						</div>

					</div>
				</div>
			</div>
		);
	}

}