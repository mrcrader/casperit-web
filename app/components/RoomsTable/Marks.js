import React from 'react';

const Marks = {
    bookedCircle: <svg height="24" width="24"><circle cx="12" cy="12" r="10" fill="#FF003A" /></svg>,
    emptyCircle: <svg height="24" width="24"><circle cx="12" cy="12" r="10" fill="#B3E5FC" /></svg>,
    onApproveCircle: <svg height="24" width="24"><circle cx="12" cy="12" r="10" fill="#03A9F4" /></svg>
}

export default Marks;
