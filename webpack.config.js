var HTMLWebpackPlugin = require('html-webpack-plugin');
var HTMLWebpackPluginConfig = new HTMLWebpackPlugin({
	template: __dirname + '/app/index.html',
	filename: 'index.html',
	inject: 'body'
});
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
	devtool: 'inline-source-map',
	entry: __dirname + '/app/index.js',
	module: {
        rules: [
			{
				test: /\.js?$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
				  presets: ["react"]
				}
			},
			{
				test:/\.(s*)css$/,
				loader: ExtractTextPlugin.extract({use:['css-loader', 'sass-loader'],
				fallback:'style-loader'})
			},
			{
				test: /\.(png|jpg|svg|gif)$/,
				use: [
				  {
					loader: 'file-loader',
					options: {},
				  },
				],
			  }
		]
	},
	output: {
		filename: 'bundle.js',
		path: __dirname + '/build'
	},
	plugins:
	[HTMLWebpackPluginConfig,
		new ExtractTextPlugin("style.boundle.css")],
};